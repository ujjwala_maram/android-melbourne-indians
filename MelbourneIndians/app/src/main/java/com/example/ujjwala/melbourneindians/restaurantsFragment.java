package com.example.ujjwala.melbourneindians;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v4.app.ListFragment;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link restaurantsFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class restaurantsFragment extends ListFragment {

    private ProgressDialog progress;
    private  boolean isNetworkAvailable;
    public static restaurantsFragment newInstance() {
        restaurantsFragment fragment = new restaurantsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public restaurantsFragment() {
        // Required empty public constructor
    }

    @Override
    public  synchronized void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("xxxxxxxxxxxxxxxxxxxxx", "VALUES: " +localStorage.RESTAURANTS.size());
        isNetworkAvailable = isNetworkAvailable(this.getActivity().getApplicationContext());
        if(localStorage.RESTAURANTS.size()== 0) {
            LoadingTask downloadTask = new LoadingTask(this.getActivity().getApplicationContext());
            downloadTask.execute("Restaurants");

        }

        setListAdapter(new RowIconAdapter());

    }

    public  void getData(String name){

        final String value = name;
        ParseQuery query = new ParseQuery(value);
        isNetworkAvailable = isNetworkAvailable(this.getActivity().getApplicationContext());
        if(isNetworkAvailable == false)
        {
            query.fromPin(value);
            Log.d("CONNECTIVITY", "false");

        }

        query.findInBackground(new FindCallback<ParseObject>() {

            public  void done(List<ParseObject> listItems, ParseException e) {
               if (e == null) {
                    for (ParseObject p : listItems) {
                        //Log.d("restaurants", p.getString("name"));
                        String name = p.getString("name");
                        String address = p.getString("Address");
                        String desc = p.getString("Description");
                        String site = p.getString("Site");
                        String phone = p.getString("Phone");
                        byte[] image = null;
                        try {
                            image = p.getParseFile("Image").getData();
                        } catch (ParseException e1) {
                            e1.printStackTrace();
                        }
                        localStorage.addRestaurants(new localStorage.Data(name, address, desc, image,site,phone));
                    }

                   setListAdapter(new RowIconAdapter());

                } else {
                    Log.d("restaurants", "Error: " + e.getMessage());
                }

            }


            class RowIconAdapter extends ArrayAdapter<localStorage.Data> {
                public RowIconAdapter() {
                    super(getActivity(), R.layout.listrow,
                            R.id.row_label, localStorage.RESTAURANTS);

                }

                public View getView(int pos, View cView, ViewGroup parent) {
                    View row = super.getView(pos, cView, parent);
                    // get current image
                    ImageView icon = (ImageView) row.findViewById(R.id.row_icon);

                   // icon.setImageResource(R.drawable.ic_launcher);

                    icon.setImageBitmap(Bitmap.createScaledBitmap(localStorage.RESTAURANTS.get(pos).getImage(), 200, 200, true));
                    return row;
                }
            }


        });

    }

    public  boolean isNetworkAvailable(Context context)
    {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivity != null)
        {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();

            if (info != null)
            {
                for (int i = 0; i < info.length; i++)
                {
                    Log.i("Class", info[i].getState().toString());
                    if (info[i].getState() == NetworkInfo.State.CONNECTED)
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }


    private class LoadingTask extends AsyncTask<String, Void, Integer> {
        // Downloading data in non-ui thread
        ProgressBar mDialog;
        public LoadingTask(Context context) {


        }
        @Override
        protected Integer doInBackground(String... name) {

            try {
                final String value = name[0];

                ParseQuery query = new ParseQuery(value);
                if (isNetworkAvailable == false) {
                    query.fromPin(value);
                    Log.d("CONNECTIVITY", "false");

                }

                query.findInBackground(new FindCallback<ParseObject>() {

                    public void done(List<ParseObject> listItems, ParseException e) {
                        if (e == null) {
                            for (ParseObject p : listItems) {
                                //Log.d("restaurants", p.getString("name"));
                                String name = p.getString("name");
                                String address = p.getString("Address");
                                String desc = p.getString("Description");
                                String site = p.getString("Site");
                                String phone = p.getString("Phone");
                                byte[] image = null;
                                try {
                                    image = p.getParseFile("Image").getData();
                                } catch (ParseException e1) {
                                    e1.printStackTrace();
                                }
                                localStorage.addRestaurants(new localStorage.Data(name, address, desc, image, site, phone));
                            }

                            setListAdapter(new RowIconAdapter());

                        } else {
                            Log.d("restaurants", "Error: " + e.getMessage());
                        }

                    }
                });
            }

                catch (Exception e) {
                Log.d("Background Task", e.toString());
            }

            return 3;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(Integer result) {
           super.onPostExecute(result);

        }
    }


    class RowIconAdapter extends ArrayAdapter<localStorage.Data> {
        public RowIconAdapter() {
            super(getActivity(), R.layout.listrow,R.id.row_label, localStorage.RESTAURANTS);

        }

        public View getView(int pos, View cView, ViewGroup parent) {
            View row = super.getView(pos, cView, parent);
            // get current image
            ImageView icon = (ImageView) row.findViewById(R.id.row_icon);
            icon.setMaxWidth(50);
            icon.setMaxHeight(50);
            icon.setImageBitmap(localStorage.RESTAURANTS.get(pos).getImage());

            return row;
        }
    }

    public void onItemSelected(String id) {
        Intent detailIntent = new Intent(this.getActivity(), detailsActivity.class);
        detailIntent.putExtra(detailFragment.ARG_ITEM_ID, id);
        startActivity(detailIntent);

    }

    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {
        super.onListItemClick(listView, view, position, id);

        onItemSelected(localStorage.RESTAURANTS.get(position).toString());
        // Notify the active callbacks interface (the activity, if the
        // fragment is attached to one) that an item has been selected.
        // mCallbacks.onItemSelected(DummyContent.ITEMS.get(position).id);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_restaurants, container, false);
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            //mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        // mListener = null;
    }


}
