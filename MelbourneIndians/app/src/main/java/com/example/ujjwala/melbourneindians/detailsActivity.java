package com.example.ujjwala.melbourneindians;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.ujjwala.melbourneindians.R;

public class detailsActivity extends FragmentActivity implements detailFragment.OnFragmentInteractionListener
         {

    private String value;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        // Show the Up button in the action bar.
        getActionBar().setDisplayHomeAsUpEnabled(true);

        if (savedInstanceState == null)
        {
            // Create the detail fragment and add it to the activity
            // using a fragment transaction.
            Bundle arguments = new Bundle();
            value = getIntent().getStringExtra(detailFragment.ARG_ITEM_ID);
            arguments.putString(detailFragment.ARG_ITEM_ID,value);

            detailFragment fragment = new detailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.detail_container, fragment)
                    .commit();
                    }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                NavUtils.navigateUpTo(this, new Intent(this, home.class));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


             @Override
             public void viewMapClicked(String address)
             {
                 Log.d("FRAGMENT-EXAMPLE", " details " + address);
                 Toast.makeText(getApplicationContext(), address, Toast.LENGTH_SHORT)
                         .show();
                 Intent i = new Intent(this, map.class);
                 i.putExtra(detailFragment.ARG_ITEM_ID,value);
                 i.putExtra("address",address);
                 startActivity(i);
             }

             @Override
             public void viewSiteClicked(String site) {
                  Log.d("FRAGMENT-EXAMPLE", "Site " + site);

                   Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(site));
                  startActivity(intent);


             }
             @Override
             public void shareAddressClicked(String address)
             {
                 Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                 sharingIntent.setType("text/plain");
                 sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Address :");
                 sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, address);
                 startActivity(sharingIntent);

             }

             @Override
             public void makeCallClicked(String phone)
             {
                 Intent intent = new Intent(Intent.ACTION_CALL);
                 intent.setData(Uri.parse("tel:" +phone));
                 startActivity(intent);
             }

         }
