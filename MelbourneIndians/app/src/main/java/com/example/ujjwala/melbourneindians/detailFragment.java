package com.example.ujjwala.melbourneindians;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link detailFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 *
 */
public class detailFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    public static final String ARG_ITEM_ID = "name";
    private localStorage.Data mItem;
    private OnFragmentInteractionListener mListener;
    private String address;
    private String site;
    private String phone;

    public detailFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
            if (getArguments().containsKey(ARG_ITEM_ID))
            {
                String value = getArguments().getString(ARG_ITEM_ID);
                mItem = localStorage.ITEM_VALUES.get(value);
            }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView= inflater.inflate(R.layout.fragment_detail, container, false);

        if (mItem != null)
        {
            ((TextView) rootView.findViewById(R.id.name)).setText(mItem.getItemName());
            ((TextView) rootView.findViewById(R.id.description)).setText(mItem.getDescription());
            address = mItem.getAddress();
            site = mItem.getSite();
            phone = mItem.getPhone();
            ((TextView) rootView.findViewById(R.id.address)).setText(mItem.getAddress());
            ((TextView) rootView.findViewById(R.id.phone)).setText(phone);
           ((ImageView) rootView.findViewById(R.id.imageView)).setImageBitmap(mItem.getImage());

        }


        return rootView;
    }


    @Override
    public void onResume()
    {
        super.onResume();
    registerForContextMenu(getView());


    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try
        {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e)
        {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }

            }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Actions");
        menu.add(0, v.getId(), 0,getResources().getString(R.string.map) );
        menu.add(0, v.getId(), 0,getResources().getString(R.string.site) );
        menu.add(0, v.getId(), 0,getResources().getString(R.string.address) );
        menu.add(0, v.getId(), 0,getResources().getString(R.string.call) );
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        if (item.getTitle().equals(getResources().getString(R.string.map)))
        {
            mListener.viewMapClicked(address);
        }

        if (item.getTitle().equals(getResources().getString(R.string.site)))
        {
            mListener.viewSiteClicked(site);
        }
        if (item.getTitle().equals(getResources().getString(R.string.address)))
        {
            mListener.shareAddressClicked(address);
        }
        if (item.getTitle().equals(getResources().getString(R.string.call)))
        {
            mListener.makeCallClicked(phone);
        }

        return super.onContextItemSelected(item);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
       public void viewMapClicked(String address);
        public void viewSiteClicked(String site);
        public void shareAddressClicked(String address);
        public void makeCallClicked(String phone);
    }

}
