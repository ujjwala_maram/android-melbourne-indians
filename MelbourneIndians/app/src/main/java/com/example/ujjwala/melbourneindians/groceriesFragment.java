package com.example.ujjwala.melbourneindians;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v4.app.ListFragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
  * to handle interaction events.
 * Use the {@link groceriesFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class groceriesFragment extends ListFragment {


    private  boolean isNetworkAvailable;
    public static groceriesFragment newInstance() {
        groceriesFragment fragment = new groceriesFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }
    public groceriesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("xxxxxxxxxxxxxxxxxxxxx", "VALUES: " + localStorage.GROCERIES.size());
        isNetworkAvailable = isNetworkAvailable(this.getActivity().getApplicationContext());
        if(localStorage.GROCERIES.size()== 0) {
            LoadingTask downloadTask = new LoadingTask(this.getActivity().getApplicationContext());
            downloadTask.execute("Groceries");

        }


        setListAdapter(new RowIconAdapter());

    }

    public  void getData(String name){

        final String value = name;
        ParseQuery query = new ParseQuery(value);
        query.findInBackground(new FindCallback<ParseObject>() {
            private localStorage localData= new localStorage();

            public  void done(List<ParseObject> listItems, ParseException e) {
                if (e == null) {
                    for (ParseObject p : listItems) {
                        Log.d("Groceries", p.getString("name"));
                        String name = p.getString("name");
                        String address = p.getString("Address");
                        String desc = p.getString("Description");
                        String site = p.getString("Site");
                        String phone = p.getString("Phone");
                        byte[] image = null;
                        try {
                            image = p.getParseFile("Image").getData();
                        } catch (ParseException e1) {
                            e1.printStackTrace();
                        }
                        localStorage.addGroceries(new localStorage.Data(name, address, desc, image, site,phone));
                    }

                    setListAdapter(new RowIconAdapter());

                } else {
                    Log.d("Groceries", "Error: " + e.getMessage());
                }

            }

            class RowIconAdapter extends ArrayAdapter<localStorage.Data> {
                public RowIconAdapter() {
                    super(getActivity(), R.layout.listrow,
                            R.id.row_label, localStorage.GROCERIES);

                }

                public View getView(int pos, View cView, ViewGroup parent) {
                    View row = super.getView(pos, cView, parent);
                    // get current image
                    ImageView icon = (ImageView) row.findViewById(R.id.row_icon);

                    // icon.setImageResource(R.drawable.ic_launcher);
                    icon.setImageBitmap(localStorage.GROCERIES.get(pos).getImage());
                    return row;
                }
            }


        });

    }


    private class LoadingTask extends AsyncTask<String, Void, Integer> {
        // Downloading data in non-ui thread
        ProgressBar mDialog;
        public LoadingTask(Context context) {


        }
        @Override
        protected Integer doInBackground(String... name) {

            try {
                final String value = name[0];

                ParseQuery query = new ParseQuery(value);
                if (isNetworkAvailable == false) {
                    query.fromPin(value);
                    Log.d("CONNECTIVITY", "false");

                }

                query.findInBackground(new FindCallback<ParseObject>() {

                    public void done(List<ParseObject> listItems, ParseException e) {
                        if (e == null) {
                            for (ParseObject p : listItems) {
                                Log.d("Groceries", p.getString("name"));
                                String name = p.getString("name");
                                String address = p.getString("Address");
                                String desc = p.getString("Description");
                                String site = p.getString("Site");
                                String phone = p.getString("Phone");
                                byte[] image = null;
                                try {
                                    image = p.getParseFile("Image").getData();
                                } catch (ParseException e1) {
                                    e1.printStackTrace();
                                }
                                localStorage.addGroceries(new localStorage.Data(name, address, desc, image, site,phone));
                            }

                            setListAdapter(new RowIconAdapter());

                        } else {
                            Log.d("Groceries", "Error: " + e.getMessage());
                        }

                    }
                });
            }

            catch (Exception e) {
                Log.d("Background Task", e.toString());
            }

            return 3;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);

        }
    }

    public  boolean isNetworkAvailable(Context context)
    {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivity != null)
        {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();

            if (info != null)
            {
                for (int i = 0; i < info.length; i++)
                {
                    Log.i("Class", info[i].getState().toString());
                    if (info[i].getState() == NetworkInfo.State.CONNECTED)
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }



    class RowIconAdapter extends ArrayAdapter<localStorage.Data> {
        public RowIconAdapter() {
            super(getActivity(), R.layout.listrow,R.id.row_label, localStorage.GROCERIES);

        }

        public View getView(int pos, View cView, ViewGroup parent) {
            View row = super.getView(pos, cView, parent);
            // get current image
            ImageView icon = (ImageView) row.findViewById(R.id.row_icon);
            icon.setMaxWidth(50);
            icon.setMaxHeight(50);
            icon.setImageBitmap(localStorage.GROCERIES.get(pos).getImage());

            return row;
        }
    }

    public void onItemSelected(String id) {
        Intent detailIntent = new Intent(this.getActivity(), detailsActivity.class);
        detailIntent.putExtra(detailFragment.ARG_ITEM_ID, id);
        startActivity(detailIntent);

    }

    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {
        super.onListItemClick(listView, view, position, id);

        onItemSelected(localStorage.GROCERIES.get(position).toString());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_groceries, container, false);
    }



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
           // mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }


}
