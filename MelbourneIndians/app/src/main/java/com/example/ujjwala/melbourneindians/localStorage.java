package com.example.ujjwala.melbourneindians;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ujjwala on 19/11/2014.
 */
public class localStorage
{
    public static  Map<String, Data> ITEM_VALUES = new HashMap<String, Data>();
    public static List<Data> RESTAURANTS = new ArrayList<Data>();
    public static List<Data> EVENTS = new ArrayList<Data>();
    public static List<Data> TEMPLES = new ArrayList<Data>();
    public static Map<String, Data> GROCERIES_VALUES = new HashMap<String, Data>();
    public static List<Data> GROCERIES = new ArrayList<Data>();

    public static void addEvents(Data item)
    {
        EVENTS.add(item);
        ITEM_VALUES.put(item.getItemName(), item);
    }


    public static void addRestaurants(Data item)
    {
        RESTAURANTS.add(item);
        ITEM_VALUES.put(item.getItemName(), item);
    }

    public static  void addTemples(Data item)
    {
        TEMPLES.add(item);
        ITEM_VALUES.put(item.getItemName(), item);
    }

    public static  void addGroceries(Data item)
    {
        GROCERIES.add(item);
        ITEM_VALUES.put(item.getItemName(), item);
    }





    public static class Data
    {
        private String name;
        private String address;
        private String description;
        private byte[] image=null;
        private String site;
        private String phone;

        public Data(String name,String address,String description,byte[] image,String site,String phone)
        {
            this.name = name;
            this.address = address;
            this.description = description;
             this.image = image;
            this.site = site;
            this.phone = phone;


        }

        public String getItemName() {
            return name;
        }

        public String getAddress() {
            return address;
        }

        public String getDescription() {
            return description;
        }

        public Bitmap getImage()
        {
           return Bitmap.createScaledBitmap(BitmapFactory.decodeByteArray(image, 0, image.length), 300, 300,true);
           // return Bitmap.createBitmap(BitmapFactory.decodeByteArray(image,0,300));

        }
        public String getPhone()
        {
            return phone;
        }

        public String getSite()
        {
            return site;
        }
        @Override
        public String toString()
        {

            return name;
        }
    }
}
