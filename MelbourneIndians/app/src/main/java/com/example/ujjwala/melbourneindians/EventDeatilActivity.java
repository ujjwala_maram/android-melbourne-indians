package com.example.ujjwala.melbourneindians;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ujjwala.melbourneindians.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class EventDeatilActivity extends FragmentActivity {

    private localStorage.Data mItem;
    private String address;
    private String site;
    private String phone;
    private GoogleMap gMap;
    private String value;
    private double latitude ;
    private double longitude;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_deatil);

            String value =  getIntent().getStringExtra(detailFragment.ARG_ITEM_ID);
            mItem = localStorage.ITEM_VALUES.get(value);
       InitalizeUI();
    }

    private void InitalizeUI()
    {
        if (mItem != null)
        {
            ((TextView) findViewById(R.id.name)).setText(mItem.getItemName());
            ((TextView)findViewById(R.id.description)).setText(mItem.getDescription());
            address = mItem.getAddress();
            site = mItem.getSite();
            phone = mItem.getPhone();
            ((TextView) findViewById(R.id.address)).setText(mItem.getAddress());
            ((TextView) findViewById(R.id.phone)).setText(phone);
            ((ImageView)findViewById(R.id.imageView)).setImageBitmap(mItem.getImage());
            if(isNetworkAvailable(this)) {
                showMapData(address, value);
            }
            else
            {
                Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
            }

        }
    }


    private void showMapData(String address, String name)
    {

        getLocationFromAddress(address);
                // default map data
        LatLng location = new LatLng(latitude, longitude);
        //LatLng circularQuaySydney = new LatLng(-33.86127, 151.2114);

        SupportMapFragment fm = (SupportMapFragment)  getSupportFragmentManager().findFragmentById(R.id.map);
        gMap = fm.getMap();
        // gMap = ((SupportMapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
        if (gMap == null)
            return;

        gMap.addMarker(new MarkerOptions()
                .position(location)
                .title(name)
                .snippet(address)
                .icon(BitmapDescriptorFactory
                        .fromResource(R.drawable.ic_location_pin)));


        gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 7));
        gMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);


        final Geocoder geocoder;

        geocoder = new Geocoder(this, Locale.getDefault());

        gMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng pos)
            {


            }
        });


    }

    public void getLocationFromAddress(String strAddress) {

        Geocoder coder = new Geocoder(this);
        List<Address> address;

        try {
            address = coder.getFromLocationName(strAddress, 5);
            Address location = address.get(0);
            latitude= location.getLatitude();
            longitude=  location.getLongitude();



        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.event_deatil, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.call) {
            makeCallClicked(phone);
        }
        else if(id == R.id.share)
        {
            shareAddressClicked(address);
        }

        switch (item.getItemId())
        {
            case android.R.id.home:
                NavUtils.navigateUpTo(this, new Intent(this, home.class));
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public  boolean isNetworkAvailable(Context context)
    {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivity != null)
        {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();

            if (info != null)
            {
                for (int i = 0; i < info.length; i++)
                {
                    Log.i("Class", info[i].getState().toString());
                    if (info[i].getState() == NetworkInfo.State.CONNECTED)
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }


    public void shareAddressClicked(String address)
    {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Address :");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, address);
        startActivity(sharingIntent);

    }


    public void makeCallClicked(String phone)
    {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" +phone));
        startActivity(intent);
    }

}
