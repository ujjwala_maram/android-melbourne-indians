package com.example.ujjwala.melbourneindians;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.ujjwala.melbourneindians.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class map extends FragmentActivity {

    private GoogleMap gMap;
    private String value;
    private double latitude ;
    private double longitude;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        if (savedInstanceState == null) {

            value = getIntent().getStringExtra(detailFragment.ARG_ITEM_ID);
        }
        Bundle arguments = getIntent().getExtras();
        // ArrayList<String> listDetails = arguments.getStringArrayList("detailList");
        boolean isNetworkAvailable = isNetworkAvailable(this.getApplicationContext());
        if(isNetworkAvailable == true)
        {
            showMapData(arguments);
        }
        else
        {
            Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
        }

    }

    public  boolean isNetworkAvailable(Context context)
    {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivity != null)
        {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();

            if (info != null)
            {
                for (int i = 0; i < info.length; i++)
                {
                    Log.i("Class", info[i].getState().toString());
                    if (info[i].getState() == NetworkInfo.State.CONNECTED)
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private void showMapData(Bundle data)
    {
        String address = data.getString("address");
       // convertAddress(address);
        Log.d("Address", address);
        getLocationFromAddress(address);
        String name = data.getString("name");
        // default map data
        LatLng location = new LatLng(latitude, longitude);
        //LatLng circularQuaySydney = new LatLng(-33.86127, 151.2114);

        SupportMapFragment fm = (SupportMapFragment)  getSupportFragmentManager().findFragmentById(R.id.map);
        gMap = fm.getMap();
        // gMap = ((SupportMapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
        if (gMap == null)
            return;

        gMap.addMarker(new MarkerOptions()
                .position(location)
                .title(name)
                .snippet(address)
                .icon(BitmapDescriptorFactory
                        .fromResource(R.drawable.ic_location_pin)));


        gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 7));
        gMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);


       final  Geocoder geocoder;

        geocoder = new Geocoder(this, Locale.getDefault());

        gMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng pos)
            {

                try {
                 List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);

                String address = addresses.get(0).getAddressLine(0);
                String city = addresses.get(0).getAddressLine(1);
                String country = addresses.get(0).getAddressLine(2);
                gMap.addMarker(new MarkerOptions()
                        .position(pos)
                        .title(address+"\n "+city+"\n "+country)
                        .visible(true)
                        .icon(BitmapDescriptorFactory
                                .fromResource(R.drawable.ic_location_pin)));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });


    }

    public void getLocationFromAddress(String strAddress) {

        Geocoder coder = new Geocoder(this);
        List<Address> address;

            try {
                address = coder.getFromLocationName(strAddress, 5);
            Address location = address.get(0);
           latitude= location.getLatitude();
          longitude=  location.getLongitude();



        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.map, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent i = new Intent(this, detailsActivity.class);
                i.putExtra(detailFragment.ARG_ITEM_ID,value);
                 NavUtils.navigateUpTo(this,i);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
